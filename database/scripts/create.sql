
CREATE DATABASE BANCO;

USE BANCO;

CREATE TABLE USUARIO(
    idusuario   INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(50) NOT NULL,
    username    VARCHAR(20) NULL,
    email       VARCHAR(30) NULL,
    password    VARCHAR(30),
    saldo       FLOAT NOT NULL,
    cuenta      INTEGER NOT NULL
);

CREATE TABLE TRANSFERENCIA(
    idtransferencia     INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    monto               FLOAT NOT NULL,
    USUARIO_Envio       INTEGER NOT NULL,
    USUARIO_Destino     INTEGER NOT NULL,
    fecha               DATE NOT NULL,
    CONSTRAINT transferencia_fk1 FOREIGN KEY ( USUARIO_Envio )
        REFERENCES USUARIO ( idusuario ),
    CONSTRAINT transferencia_fk2 FOREIGN KEY ( USUARIO_Destino )
        REFERENCES USUARIO ( idusuario )
);

CREATE TABLE CREDITO(
    idcredito              INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    monto                  FLOAT NOT NULL,
    descripcion            VARCHAR(200) NOT NULL,
    fechapedido            DATE NOT NULL,
    fechaaprobado          DATE,
    fechafinal             DATE,
    estado                 CHAR(1) NOT NULL,
    USUARIO_idusuario      INTEGER NULL,
    CONSTRAINT credito_fk1 FOREIGN KEY ( USUARIO_idusuario )
        REFERENCES USUARIO ( idusuario )
);

CREATE TABLE DEBITO(
    iddebito     INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    monto                  FLOAT NOT NULL,
    descripcion            VARCHAR(200) NOT NULL,
    estado                 CHAR(1) NOT NULL,
    USUARIO_idusuario      INTEGER NULL,
    CONSTRAINT debito_fk1 FOREIGN KEY ( USUARIO_idusuario )
        REFERENCES USUARIO ( idusuario )
);


DELIMITER $$

CREATE PROCEDURE transferencia(
    IN fuente INT, 
    IN destino INT,
    IN monto INT
)
BEGIN
	INSERT INTO transferencia(monto,usuario_envio,usuario_destino) VALUES(monto,fuente,destino);
    UPDATE usuario set saldo = saldo - monto where idusuario = fuente;
    UPDATE usuario set saldo = saldo + monto where idusuario = destino;
    SELECT * FROM usuario WHERE idusuario = fuente;
END$$

DELIMITER ;




DELIMITER $$

CREATE PROCEDURE pagosHistorial(
    IN usuario INT
)
BEGIN
	SELECT monto,fecha,u1.cuenta as origen,u2.cuenta as destino FROM TRANSFERENCIA
	INNER JOIN usuario u1 
	INNER JOIN usuario u2 
	where USUARIO_ENVIO = usuario AND u1.idusuario = USUARIO_Envio AND u2.idusuario = USUARIO_Destino;
END$$

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE cobrosHistorial(
    IN usuario INT
)
BEGIN
	SELECT monto,fecha,u1.cuenta as origen,u2.cuenta as destino FROM TRANSFERENCIA
	INNER JOIN usuario u1 
	INNER JOIN usuario u2 
	where USUARIO_Destino = usuario AND u1.idusuario = USUARIO_Envio AND u2.idusuario = USUARIO_Destino;
END$$

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE aprobarCredito(
    IN inicio DATE,
    IN fin DATE,
    IN creditoId INT
)
BEGIN
	DECLARE user INT;
    DECLARE money INT;
    
    UPDATE credito SET estado = 1, fechaaprobado = inicio, fechafinal = fin WHERE idcredito = creditoId;
    
    SELECT USUARIO_idusuario,monto INTO user, money FROM credito WHERE idcredito = creditoId;
    
	UPDATE usuario SET saldo = saldo + money WHERE idusuario = user;
    
	SELECT name,email,cuenta FROM usuario WHERE idusuario = user;
END$$

DELIMITER ;