### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.


kill pid -9
netstat -tulp | grep kubectl 
kubectl proxy --port=8080 &

kubectl autoscale deployment frontend --cpu-percent=5 --min=1 --max=5 --namespace=gitlab-managed-apps
kubectl delete hpa db frontend --namespace=gitlab-managed-apps


kubectl run --generator=run-pod/v1 -i --tty load-generator --image=busybox /bin/sh
while true; do wget -q -O- http://34.123.86.89; done

kubectl delete hpa,service,deployment php-apache
kubectl delete pod load-generator
kubectl delete hpa frontend --namespace=gitlab-managed-apps
