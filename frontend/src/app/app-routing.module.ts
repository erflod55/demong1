import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './signin/signin.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { UserGuard } from './user.guard';
import { AdminGuard } from './admin.guard';


const routes: Routes = [
  {path : 'login', component : LoginComponent},
  {path : 'signin', component : SigninComponent,},
  {path : 'home', component : HomeComponent, canActivate : [UserGuard]},
  {path : 'admin', component : AdminComponent, canActivate : [AdminGuard]},
  {path : '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
