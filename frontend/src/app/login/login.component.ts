import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private logUsr = {
    cuenta : 0,
    username : '',
    password : ''
  }

  constructor(private apiService : ApiService, private router: Router) { }

  ngOnInit(): void {
  }

  cuenta(event: any){
    this.logUsr.cuenta = event.target.value;
  }

  user(event: any) {
    this.logUsr.username = event.target.value;
  }

  password(event: any) {
    this.logUsr.password = event.target.value;
  }

  signin(){
    console.log(this.logUsr);
    if(this.logUsr.username == 'admin' && this.logUsr.password == 'admin')
    {
      alert(`Bienvenido Administrador`);
      this.router.navigate(['/admin']);
      this.apiService.setuser({},'2');
    }
    this.apiService.signin(this.logUsr).subscribe(
      (res)=>{
        console.log(res);
        if(res.status){
          var usuario = res.data[0];
          if(usuario!=null)
          {
            delete usuario.password;
            this.apiService.setuser(usuario,'1');
            alert(`Bienvenido ${usuario.name}, ${usuario.email}`);
            this.router.navigate(['/home']);
          }
          else
          {
            alert('Datos icorrectos.');
          }
        }
        else{
          alert('Inténtelo de nuevo.');
        }
      },
      (err)=>{
        console.log(err);
      }
    );
  }

}
