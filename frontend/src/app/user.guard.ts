import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  constructor(private router: Router, private apiService: ApiService){}
  
  
  canActivate(){
    let rol : any = this.apiService.getRol();
    let user : any = this.apiService.currentUser();
    
    if(user == false || rol == '0'){
      this.router.navigate(['/login']);
      return false;
    }
    else if(rol == '2'){
      this.router.navigate(['/admin']);
      return false;
    }
    return true;
  }
  
}
