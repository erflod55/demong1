import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
 
  public currentUser;
  public saldoActual;
  public titleTrans;
  public titleCred;
  public aproved:boolean;
  public historialTrans = [];
  public historialCred = [];

  


  constructor(private apiService : ApiService, private router: Router) { 
    this.currentUser = this.apiService.currentUser();
    console.log(this.currentUser);
  }

  ngOnInit(): void {}

  getPagos(){
    this.titleTrans = "Pagos";
    //TODO
  }

  getCobros(){
    this.titleTrans = "Cobros";
    //TODO
  }

  getPendientes(){
    this.titleCred = "Pendientes";
    this.apiService.pendientes().subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getAprobados(){
    this.titleCred = "Aprobados";
    this.apiService.aprobados().subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
        console.log(this.historialCred);
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getPagados(){
    this.titleCred = "Pagados";
    this.apiService.pagados().subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
        console.log(this.historialCred);
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  aprobarSolicitud(data)
  {
    console.log(data);
    let dato = { idcredito:data.idcredito };
    this.apiService.aprobarCredito(dato).subscribe(
      (data)=>{
        console.log(data);
        alert('Solicitud Aprovada.');
        this.getPendientes();
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  logout(){
    this.apiService.removeUser();
    this.router.navigate(['/login']);
  }
}
