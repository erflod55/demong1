import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  public currentUser;
  public saldoActual;
  public titleTrans;
  public titleCred;
  public historialTrans = [];
  public historialCred = [];

  private strCredito = {
    idUsuario : '',
    monto : '',
    descripcion : ''
  }

  private strTransferencia = {
    origen : '',
    destino : '',
    monto : ''
  }


  constructor(private apiService : ApiService) { 
    this.currentUser = this.apiService.currentUser();
    console.log(this.currentUser);
    this.historialTrans.length
  }

  ngOnInit(): void {
    this.strCredito.idUsuario = this.currentUser.idusuario;

    this.actualizarSaldo();
  }

  origenTransferencia(event: any) {
    this.strTransferencia.origen = event.target.value;
  }

  destinoTransferencia(event: any) {
    this.strTransferencia.destino = event.target.value;
  }

  montoTransferencia(event: any) {
    this.strTransferencia.monto = event.target.value;
  }

  credit(event: any) {
    this.strCredito.monto = event.target.value;
  }

  description(event: any) {
    this.strCredito.descripcion = event.target.value;
  }

  solicitarCredito(){
    console.log(this.strCredito);
    this.apiService.credito(this.strCredito).subscribe(
      (data) => {
        console.log(data);
        alert(`crédito solicitado de ${this.strCredito.monto} para ${this.strCredito.descripcion}`);
        this.getPendientes();
      }
    );
  }

  realizarTransferencia(){
    let transferencia = {
      origen : this.strTransferencia.origen,
      destino : this.strTransferencia.destino,
      saldo : this.strTransferencia.monto,
      iduser : this.currentUser.idusuario
    }
    this.apiService.transferencia(transferencia).subscribe(
      (data)=>{
        console.log(data);
        if(data.status){
          alert("Transferencia correcta");
          this.saldoActual = data.data.saldo;
          this.currentUser.saldo = this.saldoActual;
          console.log(this.currentUser);
        }
        else{
          alert(data.message);
        }
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  actualizarSaldo(){
    this.apiService.cuenta(this.currentUser).subscribe(
      (data) =>{
        console.log(data);
        this.currentUser = data.user[0];
        this.saldoActual = data.user[0].saldo;
      },
      (err) =>{
        console.error(err);
      }
    )
  }

  getPagos(){
    this.titleTrans = "Pagos";
    this.apiService.pagosHistorial(this.currentUser.idusuario).subscribe(
      (data)=>{
        console.log(data);
        this.historialTrans = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getCobros(){
    this.titleTrans = "Cobros";
    this.apiService.cobrosHistorial(this.currentUser.idusuario).subscribe(
      (data)=>{
        console.log(data);
        this.historialTrans = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getPendientes(){
    this.titleCred = "Pendientes";
    this.apiService.pendientesHistorial(this.strCredito.idUsuario).subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getAprobados(){
    this.titleCred = "Aprobados";
    this.apiService.aprobadosHistorial(this.strCredito.idUsuario).subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  getPagados(){
    this.titleCred = "Pagados";
    this.apiService.pagadosHistorial(this.strCredito.idUsuario).subscribe(
      (data)=>{
        console.log(data);
        this.historialCred = data;
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  pagarSolicitud(data)
  {
    console.log(data);
    let dato = { idcredito:data.idcredito };
    this.apiService.pagarCredito(dato).subscribe(
      (data)=>{
        console.log(data);
        if(data.status == true)
        {
          alert('Crédito pagado.');
          this.getAprobados();
          this.actualizarSaldo();
        }
        else
        {
          alert('Dinero insuficiente.');
        }
      },
      (err)=>{
        console.error(err);
      }
    );
  }
}
