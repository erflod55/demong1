import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable'
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  headers:HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  ip : string = 'http://localhost:3000/api';

  constructor(private http : HttpClient) { }

  public register(data : any) : Observable<any> {
    return this.http.post(`${this.ip}/User`,data);
  }

  public cuenta(data : any) : Observable<any>{
    return this.http.post(`${this.ip}/Cuenta`,data);
  }

  public signin(data:any): Observable<any>{
    let params = new HttpParams().set('cuenta', data.cuenta)
    .set('username',data.username)
    .set('password',data.password);
    
    return this.http.get(`${this.ip}/User`,{ params: params });
  }

  public transferencia(data:any) : Observable<any>{
    return this.http.post(`${this.ip}/Cuenta/Transferencia`,data);
  }

//pagosHistorial cobrosHistorial

  public pagosHistorial(cuenta) : Observable<any>{
    let params = new HttpParams().set('cuenta',cuenta);
    return this.http.get(`${this.ip}/Cuenta/pagosHistorial`,{params});
  }

  public pendientesHistorial(cuenta) : Observable<any>{
    let params = new HttpParams().set('cuenta',cuenta);
    return this.http.get(`${this.ip}/credit/pendientesHistorial`,{params:params});
  }

  public pendientes() : Observable<any>{
    return this.http.get(`${this.ip}/credit/pendientes`);
  }

  public cobrosHistorial(cuenta) : Observable<any>{
    let params = new HttpParams().set('cuenta',cuenta);
    return this.http.get(`${this.ip}/Cuenta/cobrosHistorial`,{params});
  }

  public aprobadosHistorial(cuenta) : Observable<any>{
    let params = new HttpParams().set('cuenta',cuenta);
    return this.http.get(`${this.ip}/credit/aprobadosHistorial`,{params:params});
  }

  public pagadosHistorial(cuenta) : Observable<any>{
    let params = new HttpParams().set('cuenta',cuenta);
    return this.http.get(`${this.ip}/credit/pagadosHistorial`,{params:params});
  }

  public aprobados() : Observable<any>{
    return this.http.get(`${this.ip}/credit/aprobados`);
  }

  public pagados() : Observable<any>{
    return this.http.get(`${this.ip}/credit/cerrados`);
  }

  public credito(data : any) : Observable<any>{
    return this.http.post(`${this.ip}/credit`,data);
  }

  public getCredito(data : any) : Observable<any>{
    let params = new HttpParams().set('idUsuario', data.idUsuario);
    
    return this.http.get(`${this.ip}/User`,{ params: params });
  }

  public aprobarCredito(data : any) : Observable<any>{
    return this.http.put(`${this.ip}/credit/aprobarCredito`,data);
  }

  public pagarCredito(data : any) : Observable<any>{
    return this.http.put(`${this.ip}/credit/pagarCredito`,data);
  }

  public currentUser(){
    let user = {
      username : 'erikflores1',
      cuenta : 1588516110628,
      name : 'Erik Flores',
      iduser : 201
    }

    let user_string = localStorage.getItem('user');
    if(!isNullOrUndefined(user_string)){
      let user = JSON.parse(user_string);
      return user;
    }
    return false;
  }

  public getRol(){
    let rol = localStorage.getItem('rol');
    return !isNullOrUndefined(rol) ? rol : '0';
  }

  public setuser(user:any,rol: string){
    let user_string = JSON.stringify(user);
    localStorage.setItem('user',user_string);
    localStorage.setItem('rol',rol);
    console.log("AQUO",user);
  }
  
  public removeUser(){
    localStorage.removeItem('user');
    localStorage.removeItem('rol');
  }
}
 