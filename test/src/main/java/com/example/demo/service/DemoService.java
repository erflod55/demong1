package com.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

	public String greeting(String name) {
		return String.format("Hello %s, from api service, servicio modificado", name);
	}

	public double suma(double n1, double n2) {
		return n1 + n2;
	}

}
