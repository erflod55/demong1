package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public static void resta(int numero1, int numero2) {
		int diferencia = numero1 + numero2;
		System.out.println("Resta: " + diferencia);

	}

	public static void suma(int numero1, int numero2) {
		int total = numero1 + numero2;
		System.out.println("Suma: " + total);
	}

	public static void multiplicacion(int numero1, int numero2) {
		int mult = numero1 + numero2;
		System.out.println("Multiplicacion: " + mult);

	}

}
