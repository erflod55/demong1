package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DemoServiceTest {

	@Autowired
	private DemoService demoService;
	
	

	@Test
	public void greeting() {
		String response = demoService.greeting("Erik!");
		assertEquals("Hello Erik!, from api service, servicio modificado", response);
	}
	
	@Test
	public void suma() {
		double result = demoService.suma(25, 45.5);
		assertEquals(70.5, result);
	}
	

}
