import {Router} from 'express';
import {cuentaController} from '../controller/cuenta.controller';

class UserRoute {
    router : Router = Router();

    constructor(){
        this.config();
    }

    config():void{
        this.router.post('/',cuentaController.consultaSaldo);
        this.router.post('/transferencia',cuentaController.transferencia);
        this.router.get('/pagosHistorial',cuentaController.pagosHistorial);
        this.router.get('/cobrosHistorial',cuentaController.cobrosHistorial);
    }
}

const userRoute = new UserRoute();
export default userRoute.router;