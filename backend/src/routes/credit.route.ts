import {Router} from 'express';
import {creditController} from '../controller/credit.controller';

class CreditRoute {
    router : Router = Router();

    constructor(){
        this.config();
    }

    config():void{
        this.router.post('/',creditController.create);
        this.router.get('/',creditController.get);
        this.router.get('/pendientesHistorial',creditController.pendientesHistorial);
        this.router.get('/aprobadosHistorial',creditController.aprobadosHistorial);
        this.router.get('/pagadosHistorial',creditController.pagadosHistorial);
        this.router.get('/aprobados',creditController.aprobados);
        this.router.get('/pendientes',creditController.pendientes);
        this.router.put('/aprobarCredito',creditController.aprobarCredito);
        this.router.get('/cerrados',creditController.cerrados);
        this.router.put('/pagarCredito',creditController.pagarCredito);
    }
}

const creditRoute = new CreditRoute();
export default creditRoute.router;