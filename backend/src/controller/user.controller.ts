import {Request, Response} from 'express';
import {pool} from '../database/connection';
import {sendMail} from './mail.controller';

class UserController{
    constructor(){
    }

    public async create(req: Request, res: Response){
        let {fullname, username, email, password} = req.body;
        let cuenta = Date.now();
        let saldo = 0;
        let user = [fullname,username,email,password,saldo,cuenta.toString().slice(4)];
        console.log(user);
        pool.query('INSERT INTO USUARIO(name,username,email,password,saldo,cuenta) values(?,?,?,?,?,?)',user,(err:any,data:any)=>{
            if(err){
                console.log(err);
                res.send({status : false});
            }else{
                sendMail('appmorpheus5@gmail.com',email,'Numero de Cuenta',cuenta.toString());
                res.send({status : true,cuenta,fullname,email,username});
            }
        });
    }

    public async get(req: Request, res: Response){
        console.log(req.query);
        let {cuenta, username, password} = req.query;
        let user = [cuenta,username,password];
        console.log(user);
        pool.query('SELECT * FROM USUARIO WHERE cuenta = ? AND username = ? AND password = ?',user,(err:any,data:any)=>{
            if(err){
                res.send({status : false});
            }else{
                res.send({status : true, data: data});
            }
        });
    }
}

export const userController = new UserController();