import {Request, Response} from 'express';
import {pool} from '../database/connection';
import {sendMail} from './mail.controller';

class CuentaController{
    constructor(){
    }

    public async consultaSaldo(req: Request, res: Response){
        let {username, cuenta} = req.body;
        let user = [cuenta,username];
        pool.query('SELECT name,username,email,saldo,cuenta,idusuario FROM USUARIO WHERE cuenta = ? AND username = ? ',user,(err:any,data:any)=>{
            if(err){
                res.send({status : false});
            }else{
                res.send({status : true, user: data});
            }
        });
    }

    public async transferencia(req: Request, res: Response){
        let {origen,destino,saldo,iduser} = req.body;
        pool.query('SELECT * FROM USUARIO WHERE cuenta = ? AND idusuario = ?',[origen,iduser],(err:any,data:any)=>{
            let user = data[0];
            if(user == null){
                return res.send({status:false, message: 'No existe la cuenta origen'})
            }
            if(user.saldo < saldo){
                return res.send({status : false, message : 'No posee fondos'});
            }
            pool.query('SELECT * FROM USUARIO WHERE cuenta = ?',[destino],(err:any, data:any)=>{
                let userDestino = data[0];
                if(userDestino == null){
                    return res.send({status : false,message : 'No existe la cuenta destino'});
                }                
                pool.query('CALL transferencia(?,?,?)',[iduser,userDestino.idusuario,saldo],(err:any,data:any)=>{
                    console.log(data);
                    return res.send({status : true, data: data[0][0]});
                });
            });
        });
    }

    public async pagosHistorial(req: Request, res: Response){
        let {cuenta} = req.query;
        let sql = 'CALL pagosHistorial(?)';
        pool.query(sql,[cuenta],(err: any, data:any)=>{
            res.send(data[0]);
        })
    }

    public async cobrosHistorial(req: Request, res:  Response){
        let {cuenta} = req.query;
        let sql = 'CALL cobrosHistorial(?)';
        pool.query(sql,[cuenta],(err: any, data:any)=>{
            res.send(data[0]);
        })
    }
}

export const cuentaController = new CuentaController();

/**
 * 
 * 
 * 
 */