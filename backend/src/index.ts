//Libs
import express from 'express';
import cors from 'cors';
import userRoute from './routes/user,route';
import cuentaRoute from './routes/cuenta.route';
import creditRoute from './routes/credit.route';
import {pool, keys} from './database/connection';

//Server
const app = express();

//Conf
app.set('port',process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({extended : false}));
app.use(cors());

//Routes
app.use('/api/User',userRoute);
app.use('/api/cuenta',cuentaRoute);
app.use('/api/credit',creditRoute);
app.get('/',(req, res)=>{
    res.send('Hello world desde K8S');
});

app.get('/db/:_id', (req, res)=>{
    const {_id} = req.params;
    keys.host = _id;
    console.log(keys);
    res.send({status : true});
});

//Server
app.listen(app.get('port'),()=>{
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`);
});