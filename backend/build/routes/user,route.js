"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = require("../controller/user.controller");
class UserRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/', user_controller_1.userController.create);
        this.router.get('/', user_controller_1.userController.get);
    }
}
const userRoute = new UserRoute();
exports.default = userRoute.router;
