"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//Libs
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const user_route_1 = __importDefault(require("./routes/user,route"));
const cuenta_route_1 = __importDefault(require("./routes/cuenta.route"));
const credit_route_1 = __importDefault(require("./routes/credit.route"));
const connection_1 = require("./database/connection");
//Server
const app = express_1.default();
//Conf
app.set('port', process.env.PORT || 3000);
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(cors_1.default());
//Routes
app.use('/api/User', user_route_1.default);
app.use('/api/cuenta', cuenta_route_1.default);
app.use('/api/credit', credit_route_1.default);
app.get('/', (req, res) => {
    res.send('Hello world desde K8S');
});
app.get('/db/:_id', (req, res) => {
    const { _id } = req.params;
    connection_1.keys.host = _id;
    console.log(connection_1.keys);
    res.send({ status: true });
});
//Server
app.listen(app.get('port'), () => {
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`);
});
