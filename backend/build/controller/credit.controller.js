"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../database/connection");
const mail_controller_1 = require("./mail.controller");
class CreditController {
    constructor() {
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { monto, descripcion, idUsuario } = req.body;
            let fechaPedido = new Date();
            let credit = [monto, descripcion, fechaPedido, false, idUsuario];
            console.log(credit);
            connection_1.pool.query('INSERT INTO CREDITO(monto,descripcion,fechapedido,estado,USUARIO_idusuario) values(?,?,?,?,?)', credit, (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    console.log(data);
                    res.send({ status: true });
                }
            });
        });
    }
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { idUsuario } = req.query;
            let credit = [idUsuario];
            connection_1.pool.query('SELECT * FROM CREDITO WHERE USUARIO_idusuario = ?', credit, (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    res.send({ status: true, data: data });
                }
            });
        });
    }
    aprobadosHistorial(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { cuenta } = req.query;
            let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 1';
            connection_1.pool.query(sql, [cuenta], (err, data) => {
                res.send(data);
            });
        });
    }
    pendientesHistorial(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { cuenta } = req.query;
            let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 0';
            connection_1.pool.query(sql, [cuenta], (err, data) => {
                res.send(data);
            });
        });
    }
    pagadosHistorial(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { cuenta } = req.query;
            let sql = 'SELECT * FROM CREDITO WHERE USUARIO_idusuario = ? AND ESTADO = 2';
            connection_1.pool.query(sql, [cuenta], (err, data) => {
                res.send(data);
            });
        });
    }
    aprobados(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM CREDITO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 1';
            connection_1.pool.query(sql, (err, data) => {
                if (err) {
                    console.log(err);
                }
                else
                    res.send(data);
            });
            console.log('works');
        });
    }
    pendientes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM USUARIO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 0';
            connection_1.pool.query(sql, (err, data) => {
                res.send(data);
            });
        });
    }
    cerrados(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = 'SELECT monto, descripcion, USUARIO_idusuario, (SELECT name FROM USUARIO WHERE idusuario = USUARIO_idusuario) AS nombre, estado, fechafinal, fechapedido, idcredito FROM CREDITO WHERE ESTADO = 2';
            connection_1.pool.query(sql, (err, data) => {
                res.send(data);
            });
        });
    }
    aprobarCredito(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { idcredito } = req.body;
            let fechaaproved = new Date();
            let fechafinal = new Date();
            fechafinal.setDate(fechafinal.getDate() + 30);
            connection_1.pool.query('CALL aprobarCredito(?,?,?)', [fechaaproved, fechafinal, idcredito], (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    console.log(data[0][0]);
                    mail_controller_1.sendMail('appmorpheus5@gmail.com', data[0][0].email, 'Credito aprobado', 'Su credio ha sido aprobado');
                    res.send({ status: true, data: data });
                }
            });
        });
    }
    pagarCredito(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { idcredito } = req.body;
            connection_1.pool.query('CALL pagarCredito(?)', [idcredito], (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    console.log(data);
                    if (data[0][0].estado == 1) {
                        res.send({ status: true });
                    }
                    else {
                        res.send({ status: false });
                    }
                }
            });
        });
    }
}
exports.creditController = new CreditController();
