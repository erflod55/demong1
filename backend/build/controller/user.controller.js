"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../database/connection");
const mail_controller_1 = require("./mail.controller");
class UserController {
    constructor() {
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let { fullname, username, email, password } = req.body;
            let cuenta = Date.now();
            let saldo = 0;
            let user = [fullname, username, email, password, saldo, cuenta.toString().slice(4)];
            console.log(user);
            connection_1.pool.query('INSERT INTO USUARIO(name,username,email,password,saldo,cuenta) values(?,?,?,?,?,?)', user, (err, data) => {
                if (err) {
                    console.log(err);
                    res.send({ status: false });
                }
                else {
                    mail_controller_1.sendMail('appmorpheus5@gmail.com', email, 'Numero de Cuenta', cuenta.toString());
                    res.send({ status: true, cuenta, fullname, email, username });
                }
            });
        });
    }
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.query);
            let { cuenta, username, password } = req.query;
            let user = [cuenta, username, password];
            console.log(user);
            connection_1.pool.query('SELECT * FROM USUARIO WHERE cuenta = ? AND username = ? AND password = ?', user, (err, data) => {
                if (err) {
                    res.send({ status: false });
                }
                else {
                    res.send({ status: true, data: data });
                }
            });
        });
    }
}
exports.userController = new UserController();
